﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class CounterAnimation : MonoBehaviour
{
    [SerializeField] public UnityEvent OnFinished;

    [SerializeField] private float _duration = 0.7f;
    
    private TMP_Text _counterText;
    private float _timer = 0f;
    private bool _active = false;
    private int _startValue;
    private int _endValue;

    void Awake()
    {
        _counterText = GetComponent<TMP_Text>();
    }

    void Update()
    {
        if (_active)
        {
            _timer += Time.deltaTime;
            if (_timer > _duration)
            {
                _active = false;
                _timer = _duration;
                OnFinished.Invoke();
            }
            _counterText.SetText( (_startValue + (int)((_endValue - _startValue) * _timer / _duration)).ToString() );
        }
    }

    public void HandleOnChanged(int startValue, int endValue)
    {
        _timer = 0f;
        _startValue = startValue;
        _endValue = endValue;
        _active = true;
    }
}
