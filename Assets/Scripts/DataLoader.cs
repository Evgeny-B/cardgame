﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoader : MonoBehaviour
{
    public JSONLoader JsonLoader;

    [SerializeField] private string _folder;

    void Awake()
    {
        JsonLoader = new JSONLoader(_folder);
    }
}
