public class Card
{
    public string Title { get; private set; }
    public string Desc { get; private set; }
    public int Attack { get; private set; }
    public int HP { get; private set; }
    public int Mana { get; private set; }

    public Card(string cardName, ILoadCard dataReader)
    {
        (Title, Desc, Attack, HP, Mana) = dataReader.LoadCard(cardName);
    }
}