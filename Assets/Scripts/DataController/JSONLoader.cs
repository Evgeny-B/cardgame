using System;
using System.IO;
using UnityEngine;

public class JSONLoader : ILoadCard
{
    private string _folder;

    public JSONLoader(string folder)
    {
        _folder = folder;
    }

    public (string, string, int, int, int) LoadCard(string cardName)
    {
        string jsonString = File.ReadAllText(_folder + cardName);
        CardParams data = JsonUtility.FromJson<CardParams>(jsonString);

        return (data.Title, data.Desc, data.Attack, data.HP, data.Mana);
    }
}

[Serializable]
public struct CardParams
{
    public string Title;
    public string Desc;
    public int Attack;
    public int HP;
    public int Mana;
}