public interface ILoadCard
{
    (string, string, int, int, int) LoadCard(string cardName);
}