﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.Networking;

[Serializable] public class IntIntEvent : UnityEvent<int, int> { }

public class CardBehaviour : MonoBehaviour
{
    public IntIntEvent OnManaChanged;
    public IntIntEvent OnHPChanged;
    public IntIntEvent OnAttackChanged;
    public event Action<CardBehaviour> OnDie;

    public Card Card { get; private set; }
    public int CurrentMana { get; private set; }
    public int CurrentAttack { get; private set; }
    public int CurrentHP { get; private set; }

    public CounterAnimation HPCounter = null;
    public CounterAnimation ManaCounter = null;
    public CounterAnimation AttackCounter = null;

    [SerializeField] private TMP_Text _titleText;
    [SerializeField] private TMP_Text _descText;
    [SerializeField] private TMP_Text _manaText;
    [SerializeField] private TMP_Text _attackText;
    [SerializeField] private TMP_Text _HPText;
    [SerializeField] private SpriteRenderer _sprImage;
    
    public void Init(Card card)
    {
        Card = card;
        CurrentMana = Card.Mana;
        CurrentAttack = Card.Attack;
        CurrentHP = Card.HP;
        _titleText.SetText(Card.Title);
        _descText.SetText(Card.Desc);
        _manaText.SetText(CurrentMana.ToString());
        _attackText.SetText(CurrentAttack.ToString());
        _HPText.SetText(CurrentHP.ToString());
        StartCoroutine(DownloadImage("https://picsum.photos/536/354"));

        if (HPCounter == null) 
            OnHPChanged.AddListener(HandleHPChange);
        else
            OnHPChanged.AddListener(HPCounter.HandleOnChanged);
        if (ManaCounter == null) 
            OnManaChanged.AddListener(HandleManaChange);
        else
            OnManaChanged.AddListener(ManaCounter.HandleOnChanged);
        if (AttackCounter == null) 
            OnAttackChanged.AddListener(HandleAttackChange);
        else
            OnAttackChanged.AddListener(AttackCounter.HandleOnChanged);
    }

    IEnumerator DownloadImage(string mediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(mediaUrl);

        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) 
            Debug.Log(request.error);
        else
        {
            var texture = DownloadHandlerTexture.GetContent(request);
            if (_sprImage != null)
                _sprImage.sprite = Sprite.Create(texture, new Rect(0, 0, 300, 285), new Vector2(0.5f, 0.5f));
        }
    } 

    public void RandomAttack()
    {
        int damage = UnityEngine.Random.Range(2, 10);

        int typeAttack = UnityEngine.Random.Range(0, 3);

        if (typeAttack == 0)
            DamageHP();
        else if (typeAttack == 1)
            DamageMana();
        else
            DamageAttack();

    }

    public void DamageHP()
    {
        Debug.Log("HP damage");
        int damage = UnityEngine.Random.Range(2, 10);

        int previousCurrentHP = CurrentHP;

        CurrentHP -= damage;

        OnHPChanged.Invoke(previousCurrentHP, CurrentHP);

        if ((HPCounter == null) && (CurrentHP <= 0)) Die();
    }

    public void DamageMana()
    {
        Debug.Log("Mana damage");
        int damage = UnityEngine.Random.Range(2, 10);

        int previousCurrentMana = CurrentMana;

        CurrentMana -= damage;

        if (CurrentMana < 0) CurrentMana = 0;

        OnManaChanged.Invoke(previousCurrentMana, CurrentMana);
    }

    public void DamageAttack()
    {
        Debug.Log("Attack damage");
        int damage = UnityEngine.Random.Range(2, 10);

        int previousCurrentAttack = CurrentAttack;

        CurrentAttack -= damage;

        if (CurrentAttack < 0) CurrentAttack = 0;

        OnAttackChanged.Invoke(previousCurrentAttack, CurrentAttack);
    }

    public void HandleHPChange(int s, int e)
    {
        _HPText.SetText(CurrentHP.ToString());
    }

    public void HandleManaChange(int s, int e)
    {
        _manaText.SetText(CurrentMana.ToString());
    }

    public void HandleAttackChange(int s, int e)
    {
        _attackText.SetText(CurrentAttack.ToString());
    }

    public void HandleHPCounterFinished()
    {
        if (CurrentHP <= 0) Die();
    }

    public void Die()
    {
        OnDie?.Invoke(this);
        Destroy(this.gameObject);
    }
}
