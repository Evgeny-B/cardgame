﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonAttack : MonoBehaviour
{
    private Button _btn;

    void Awake()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(HandleOnClick);
    }

    public void HandleOnClick()
    {
        _btn.interactable = false;
    }

    public void HandleAttackEnd()
    {
        _btn.interactable = true;
    }
}
