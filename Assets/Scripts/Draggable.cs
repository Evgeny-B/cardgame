﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CardBehaviour))]
public class Draggable : MonoBehaviour
{
    public event Action OnDragBegin;
    public event Action OnDragEnd;

    public bool IsDraggable = true;
    
    [SerializeField] private DropPanel panel;

    private Collider2D _container;
    private Camera _mainCamera; 
    private Vector3 _mOffset;
    private Vector3 _initialCardPosition;
    private Collider2D _collider;
    private bool _isDragging = false;

    void Start()
    {
        _mainCamera = Camera.main;
        _collider = GetComponent<Collider2D>();
        if (panel != null) _container = panel.GetComponent<Collider2D>();
    }

    Vector3 GetMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        return _mainCamera.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDown()
    {
        if (IsDraggable)
        {
            Collider2D hit = Physics2D.OverlapPoint((Vector2)GetMouseWorldPos());
     
            if (hit == _collider)
            {
                _mOffset = transform.position - GetMouseWorldPos();
                _isDragging = true;
                _initialCardPosition = transform.position;
                OnDragBegin?.Invoke();
            }
        }
    }

    void OnMouseDrag()
    {
        if (_isDragging) transform.position = GetMouseWorldPos() + _mOffset;
    }

    void OnMouseUp()
    {
        if ((IsDraggable) && (_isDragging))
        {
            _isDragging = false;
            if ( (_container != null) && ( _container.OverlapPoint(GetMouseWorldPos()) ) )
            {
                panel.AddCard(GetComponent<CardBehaviour>());
                IsDraggable = false;
            }
            else
            {
                transform.position = _initialCardPosition;
            }
            OnDragEnd?.Invoke();
        }
    }
}
