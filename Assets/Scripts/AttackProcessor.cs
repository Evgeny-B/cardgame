﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

[RequireComponent(typeof(CardInHandCollection))]
public class AttackProcessor : MonoBehaviour
{
    public UnityEvent OnAttackEnd;

    [SerializeField] private int _attackingCardNumber = 0;
    [SerializeField] private float _duration = 0.5f;

    private CardInHandCollection _collection;
    private Vector3 _initialCardPosition;
    private CardBehaviour _attackingCard;

    void Awake()
    {
        _collection = GetComponent<CardInHandCollection>();
        _collection.OnCardRemoved += HandleCardRemoved;
    }

    void Start()
    {
        foreach (CardBehaviour card in _collection.Collection)
        {
            if (card.HPCounter != null)
                card.HPCounter.OnFinished.AddListener(HandlePointsChangeFinished);
            else
                card.OnHPChanged.AddListener(HandlePointsChangeFinished);
            if (card.ManaCounter != null)
                card.ManaCounter.OnFinished.AddListener(HandlePointsChangeFinished);
            else
                card.OnManaChanged.AddListener(HandlePointsChangeFinished);
            if (card.AttackCounter != null)
                card.AttackCounter.OnFinished.AddListener(HandlePointsChangeFinished);
            else
                card.OnAttackChanged.AddListener(HandlePointsChangeFinished);
        }
    }

    public void Attack(CardBehaviour card)
    {                
        card.RandomAttack();

        _attackingCardNumber += 1;
        if (_attackingCardNumber >= _collection.Collection.Count) _attackingCardNumber = 0;
    }

    public void StartAttackMotion()
    {
        if (_collection.Collection.Count == 0) return;
        _attackingCard = _collection.Collection[_attackingCardNumber];

        var draggable = _attackingCard.GetComponent<Draggable>();
        if (draggable != null) draggable.IsDraggable = false;

        _initialCardPosition = _attackingCard.transform.position;
        Vector3 target = new Vector3(0,0,0);
        _attackingCard.transform.DOMove(target, _duration)
            .OnComplete(() => {
                Attack(_attackingCard);
                if (_attackingCard.CurrentHP <= 0)
                    _attackingCard.OnDie += HandleCardDie;
            });
    }

    public void EndAttackMotion()
    {
        if (_attackingCard.CurrentHP > 0) 
            _attackingCard.transform.DOMove(_initialCardPosition, _duration)
               .OnComplete(EndAttack);
        else
            EndAttack();
    }

    void EndAttack()
    {
        OnAttackEnd.Invoke();
        var draggable = _attackingCard.GetComponent<Draggable>();
        if (draggable != null) draggable.IsDraggable = true;
    }

    public void HandleCardRemoved(int index)
    {
        if ( (_attackingCardNumber > 0) && (index < _attackingCardNumber) ) 
            _attackingCardNumber -= 1;
        if (_attackingCardNumber >= _collection.Collection.Count)
            _attackingCardNumber = 0;
    }

    public void HandlePointsChangeFinished()
    {
        EndAttackMotion();
    }


    public void HandlePointsChangeFinished(int s, int e)
    {
        EndAttackMotion();
    }

    public void HandleCardDie(CardBehaviour card)
    {
        card.OnDie -= HandleCardDie;
        EndAttackMotion();
    }
}
