﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable] public class CardBehaviourEvent : UnityEvent<CardBehaviour> { }

public class DropPanel : MonoBehaviour
{
    public CardBehaviourEvent OnPanelDrop;

    private List<CardBehaviour> _cardsOnPanel;

    void Awake()
    {
        _cardsOnPanel = new List<CardBehaviour>();
    }

    public void AddCard(CardBehaviour card)
    {
        _cardsOnPanel.Add(card);
        PutCard(_cardsOnPanel.Count - 1);
        card.transform.parent = transform;
        OnPanelDrop.Invoke(card);
    }

    void PutCard(int i)
    {
        if (i >= _cardsOnPanel.Count) return;
        _cardsOnPanel[i].transform.position = new Vector3(-2.45f + i, 1.8f, -4 - 0.01f*i);
        _cardsOnPanel[i].transform.rotation = Quaternion.identity;
    }
}
