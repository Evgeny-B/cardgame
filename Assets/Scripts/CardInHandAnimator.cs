﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

[RequireComponent(typeof(CardInHandCollection))]
public class CardInHandAnimator : MonoBehaviour
{
    [SerializeField] private float _duration = 0.7f;
    private CardInHandCollection _collection;

    void Awake()
    {
        _collection = GetComponent<CardInHandCollection>();
        _collection.OnCardRemoved += HandleCardRemoved;
    }

    void Start()
    {
        ShowCardsInArc();
    }

    void ShowCardsInArc()
    {
        int numCards = _collection.Collection.Count;
        if (numCards <= 0) return;

        float arc = numCards >= 6 ? 32.0f : 4f*numCards + 8f;
        float angleDelta = 0f;
        if (numCards > 1) angleDelta = arc / (numCards - 1);

        for (int i = 0; i < numCards; i++)
        {
            float angle = arc/2 - angleDelta*i;
            _collection.Collection[i].transform.DOMove(new Vector3(i - numCards/2 + 0.25f, -4.92f + 40.0f * (Mathf.Cos(angle/180) - Mathf.Cos(arc/360)), -0.01f*i), _duration);
            //_collection.Collection[i].transform.eulerAngles = new Vector3(0, 0, angle);
            _collection.Collection[i].transform.DORotate(new Vector3(0, 0, angle), _duration);
        }
    }

    public void HandleCardRemoved(int index)
    {
        ShowCardsInArc();
    }
}
