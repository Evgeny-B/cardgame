using System;
using System.Collections.Generic;
using UnityEngine;

public class CardInHandCollection : MonoBehaviour
{
    public event Action<int> OnCardRemoved;

    public List<CardBehaviour> Collection { get; private set; }

    [SerializeField] private DataLoader _dataLoader;
    [SerializeField] private GameObject _cardPrefab;

    void Awake()
    {
        Collection = new List<CardBehaviour>();
    }

    void Start()
    {
        int numCards = UnityEngine.Random.Range(4, 7);

        for (int i = 0; i < numCards; i++)
        {
            GameObject cardGameObject = Instantiate(_cardPrefab, new Vector3(0, -4.92f, 0), Quaternion.identity);
            cardGameObject.SetActive(true);
            CardBehaviour cardBehaviour = cardGameObject.GetComponent<CardBehaviour>();
            cardBehaviour.Init(new Card(UnityEngine.Random.Range(1, 6).ToString() + ".json", _dataLoader.JsonLoader));
            cardBehaviour.transform.parent = transform;
            Collection.Add(cardBehaviour);
            cardBehaviour.OnDie += HandleCardDie;
        }
    }

    void RemoveCard(CardBehaviour card)
    {
        int index = Collection.IndexOf(card);
        if (Collection.Remove(card))
            OnCardRemoved?.Invoke(index);
    }

    public void HandleCardDie(CardBehaviour card)
    {
        if (Collection.Contains(card))
        {
            card.OnDie -= HandleCardDie;
            RemoveCard(card);
        }
    }

    public void HandleOnPanelDrop(CardBehaviour card)
    {
        RemoveCard(card);
    }
}