﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Draggable))]
public class Glowing : MonoBehaviour
{
    [SerializeField] private GameObject _glow;

    private Draggable _draggableComponent;

    void Awake()
    {
        _draggableComponent = GetComponent<Draggable>();
        _draggableComponent.OnDragBegin += HandleDragBegin;
        _draggableComponent.OnDragEnd += HandleDragEnd;
    }

    public void HandleDragBegin()
    {
        _glow.SetActive(true);
    }

    public void HandleDragEnd()
    {
        _glow.SetActive(false);
    }

    void OnDestroy()
    {
        _draggableComponent.OnDragBegin -= HandleDragBegin;
        _draggableComponent.OnDragEnd -= HandleDragEnd;
    }
}
